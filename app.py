import json, re
from ms import mssql
from datetime import datetime

def fileWrite(name, message):
    with open(name,"a") as f:
        f.write(str(message) + "\n")
        f.close()

def printCsv(row):
    strRow = ''
    for col in row:
        strRow +=  str(col) + ','
    fileWrite('out.csv', strRow)


if __name__ == '__main__':
    with open('config.json') as fileJSON:
        config = json.load(fileJSON)

        try:
            con = mssql.SqlConnection(config['host'], config['user'], config['pwd'], config['db'])
            con.open()

            if con.StatusError:
                fileWrite('erro.log', con.MessageError)
                exit()
            
            with open('query.sql') as queryFile:
                query = queryFile.read()
                
                
                if re.search(r'^SELECT(.*)', query.upper().lstrip()):
                    con.cursor.execute(query)
                    row = con.cursor.fetchone()
                    while row:
                        printCsv(row)
                        row = con.cursor.fetchone()
                    queryFile.close()
                elif re.search(r'^UPDATE(.*)', query.upper().lstrip()):
                    con.cursor.execute(query)
                    con.cnx.commit()
                else:
                    fileWrite('erro.log', '[' + str(datetime.now()) + '] Consuta no valida: ' + query )

                
        except Exception as err:
            fileWrite('erro.log', err)
        con.close()
